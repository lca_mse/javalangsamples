/*############################################################################
## Copyright (c) logi.cals. All rights reserved.
## ---------------------------------------------------------------------------
## TASK      :: Junit 3.2 Style Test
##
## ---------------------------------------------------------------------------
## AUTHOR    :: Mario Semo
## CREATED   :: 13-01-10
## ---------------------------------------------------------------------------
## NOTES     :: none
## ---------------------------------------------------------------------------
############################################################################*/

//*****************************************************************************
// Package
//*****************************************************************************
package com.logicals.ms.javasamples;

//*****************************************************************************
// Imports
//*****************************************************************************
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.math.*;

import com.logicals.ms.javasamples.MathSample1;

//****************************************************************************
// Classes
//****************************************************************************

/**
 * Unit test for simple App.
 */
public class AppTest
    extends TestCase
{

	  @Override public void setUp() throws Exception
	  {
	    //meineKlasse1 = new MeineKlasse();
	    //assertEquals( "Anfangs darf kein Job gesetzt sein.",null, meineKlasse1.getJob() );
	  }

	  @Override public void tearDown() throws Exception
	  {
	    //meineKlasse1 = null;
	  }
	
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	assertTrue(MathSample1.test1(3).equals(new BigInteger("2")));
    	//assertTrue(MathSample1.test1(4).equals(new BigInteger("2")));
    }


    public void testFibo1()
    {
    	assertTrue(MathSample1.test1(1).equals(new BigInteger("1")));
    	assertTrue("1st Fibonacci Number must be 1",MathSample1.test1(1).equals(new BigInteger("1")));
    }
    public void testFibo2()
    {
    	assertEquals("2nd Fibonacci Number must be 1",MathSample1.test1(2),new BigInteger("1"));
    }
    public void testFibo0()
    {
        try
        {
        	MathSample1.test1(0);
            fail( "Fibonacci Index <= 0 muss IllegalArgumentException ergeben" );
        }
        catch (IllegalArgumentException ex1)
        {
        	// ok
        }
        catch (Exception ex2)
        {
            fail( "Fibonacci Index <= 0 muss IllegalArgumentException ergeben" );
        }
    }
    public void testFibo3()
    {
    	assertEquals("3rd Fibonacci Number must be 2",MathSample1.test1(3),new BigInteger("2"));
    }
    public void testFibo100()
    {
    	assertEquals("100st Fibonacci Number must be 354224848179261915075",MathSample1.test1(100),new BigInteger("354224848179261915075"));
    }

    // will fail!!!
    public void testFibo4bug()
    {
    	//assertEquals("4th Fibonacci Number must be 3",MathSample1.test1(4),new BigInteger("2"));
    }


}
