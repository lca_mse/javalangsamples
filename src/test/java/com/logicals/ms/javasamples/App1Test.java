/*############################################################################
## Copyright (c) logi.cals. All rights reserved.
## ---------------------------------------------------------------------------
## TASK      :: Junit 4.0 Style Test
##
## ---------------------------------------------------------------------------
## AUTHOR    :: Mario Semo
## CREATED   :: 13-01-10
## ---------------------------------------------------------------------------
## NOTES     :: none
## ---------------------------------------------------------------------------
############################################################################*/

//*****************************************************************************
// Package
//*****************************************************************************
package com.logicals.ms.javasamples;

//*****************************************************************************
// Imports
//*****************************************************************************
import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.logicals.ms.javasamples.MathSample1;

//****************************************************************************
// Classes
//****************************************************************************

/**
 * @author ms
 *
 */
public class App1Test {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{

	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
   @SuppressWarnings("PMD.SystemPrintln")
	public void setUp() throws Exception
	{
		System.out.println("Setup");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
   @SuppressWarnings("PMD.SystemPrintln")
	public void tearDown() throws Exception
	{
		System.out.println("TearDown");
	}


	 @Test
	 public void testApp()
	    {
	    	assertTrue(MathSample1.test1(3).equals(new BigInteger("2")));
	    	//assertTrue(MathSample1.test1(4).equals(new BigInteger("2")));
	    }
	
	    @Test
	    public void testFibo1()
	    {
	    	assertTrue(MathSample1.test1(1).equals(new BigInteger("1")));
	    	assertTrue("1st Fibonacci Number must be 1",MathSample1.test1(1).equals(new BigInteger("1")));
	    }
	
	    @Test
	    public void testFibo2()
	    {
	    	assertEquals("2nd Fibonacci Number must be 1",MathSample1.test1(2),new BigInteger("1"));
	    }
	
	    @Test(expected=IllegalArgumentException.class)
	    public void testFibo0()
	    {
	      MathSample1.test1(0);
	    }
	
	    @Test
	    public void testFibo3()
	    {
	    	assertEquals("3rd Fibonacci Number must be 2",MathSample1.test1(3),new BigInteger("2"));
	    }
	
	    @Test
	    public void testFibo100()
	    {
	    	assertEquals("100st Fibonacci Number must be 354224848179261915075",MathSample1.test1(100),new BigInteger("354224848179261915075"));
	    }

	    // will fail!!!
	    @Test
	    public void testFibo4bug()
	    {
	    	//assertEquals("4th Fibonacci Number must be 3",MathSample1.test1(4),new BigInteger("2"));
	    }
}
