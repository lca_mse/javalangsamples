/*############################################################################
## Copyright (c) logi.cals. All rights reserved.
## ---------------------------------------------------------------------------
## TASK      :: Samples
##
## ---------------------------------------------------------------------------
## AUTHOR    :: Mario Semo
## CREATED   :: 13-01-02
## ---------------------------------------------------------------------------
## NOTES     :: none
## ---------------------------------------------------------------------------
############################################################################*/

//*****************************************************************************
// Package
//*****************************************************************************
package com.logicals.ms.javasamples;

//*****************************************************************************
// Imports
//*****************************************************************************
import java.util.*;
//import org.apache.log4j.*;

//****************************************************************************
// Classes
//****************************************************************************

/** App
*
* @author Mario Semo (MS)
* @see <reference>
* @version 1.0
* @since 13-01-02 - 20:22:51
*/
public final class MainApp
{
  // private static final Logger smLogger = Logger.getLogger(LogSample1.class.getName());

  /** main entry point to application.
  *
  * none
  *
  * @param  argc     number of arguments
  * @param  argv[]   array of argument strings
  * @return int      program error code
  * @see
  */
  @SuppressWarnings("PMD.SystemPrintln")
  public static void main(String[] pArgList)
   {
    System.out.println("F100="+MathSample1.test1(100).toString());
    GenericClass<Integer> lInt=new GenericClass<>();
    lInt.setmMember(3);
    System.out.println(lInt.getmMember());
    showProp();
   }
  @SuppressWarnings("PMD.SystemPrintln")
  private static void showProp()
  {
	  Properties lSysProp = System.getProperties();
	  Enumeration<?> lPropNames = lSysProp.propertyNames();
	  while (lPropNames.hasMoreElements())
	  {
	   String lPropName = (String) lPropNames.nextElement();
	   System.out.println(lPropName+"="+System.getProperty(lPropName));
	  }
  }

  private MainApp()
  {
  }
}
