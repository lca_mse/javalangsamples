/*############################################################################
## Copyright (c) logi.cals. All rights reserved.
## ---------------------------------------------------------------------------
## TASK      ::
## ---------------------------------------------------------------------------
## AUTHOR    :: Mario Semo
## CREATED   :: 13-01-02
## ---------------------------------------------------------------------------
## NOTES     :: none
## ---------------------------------------------------------------------------
############################################################################*/

//*****************************************************************************
// Package
//*****************************************************************************
package com.logicals.ms.javasamples;

//*****************************************************************************
// Imports
//*****************************************************************************
import java.math.*;

//****************************************************************************
// Classes
//****************************************************************************

/** MathSample1.
* @author Mario Semo (MS)
* @see <reference>
* @version 1.0
* @since 13-01-02 - 19:51:49
*/
public final class MathSample1
{
   /** BigInteger Fibonacci Numbers
   * @param  pNum     Fibonacci Number index
   * @return BigInteger  the FibonacciNumber with index pNum
   * @see
   */
   @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
   public static BigInteger test1(int pNum)
   // throws IllegalArgumentException
   // CheckStyle ueberfluessige throws-Deklaration: 'IllegalArgumentException' ist eine unchecked Exception (abgeleitet von RuntimeException).
   {
	  if (pNum<=0)
	  {
	      throw new IllegalArgumentException("Fibonacci Index<=0");
	  }
	  if (pNum<=2)
	  {
		  return BigInteger.ONE;
	  }
		
      BigInteger lV1=BigInteger.ONE;
      BigInteger lV2=BigInteger.ONE;

      for (int i=2;i<pNum;i++)
      {
         BigInteger lV3=lV1.add(lV2);
         lV1=lV2;
         lV2=lV3;
         //if (false)
         //    {
         //     System.out.println(lV3.toString());
         //    }
      }
      return lV2;
   }

   private MathSample1()
   {
   }
}
