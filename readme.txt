@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: Sample1
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-12-30
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ----------------------------------------------------------------------
@REM #######################################################################*/

Shows some language features, some APIS and contains JUNIT tests.
One of the tests fails by intention.

bin\build ... compiles, without UNIT Tests
bin\build junit ... compiles, with UNIT Tests ==>

    ....
    -------------------------------------------------------
     T E S T S
    -------------------------------------------------------
    Running com.lca_MS.samples.AppTest
    Tests run: 7, Failures: 1, Errors: 0, Skipped: 0, Time elapsed: 0.032 sec <<< FAILURE!

    Results :

    Failed tests:   testFibo4bug(com.lca_MS.samples.AppTest): 4th Fibonacci Number must be 3 expected:<3> but was:<2>

    Tests run: 7, Failures: 1, Errors: 0, Skipped: 0

    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD FAILURE
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1.297s
    [INFO] Finished at: Wed Jan 02 23:08:48 CET 2013
    [INFO] Final Memory: 6M/15M
    [INFO] ------------------------------------------------------------------------
    [ERROR] Failed to execute goal org.apache.maven.plugins:maven-surefire-plugin:2.10:test (default-test) on project JavaLangSamples: There are test failures.
    [ERROR]
    [ERROR] Please refer to D:\LCo3\MS_Java_Samples\JavaLangSamples\target\surefire-reports for the individual test results.
    [ERROR] -> [Help 1]
    [ERROR]
    [ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
    [ERROR] Re-run Maven using the -X switch to enable full debug logging.
    [ERROR]
    [ERROR] For more information about the errors and possible solutions, please read the following articles:
    [ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoFailureException
